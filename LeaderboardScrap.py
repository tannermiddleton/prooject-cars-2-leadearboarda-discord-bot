# https://github.com/Rapptz/discord.py/blob/async/examples/reply.py
import discord
import requests
from lxml import html
import steamapi
import json

TOKEN = 'NDMxNjEzNzgzMTQ4ODU1Mjk2.DahTrw.2qdy12ejJcZoh71JEAMDMfDQTi4'

client = discord.Client()

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    if message.content.startswith('!hello'):
        msg = 'Hello {0.author.mention}'.format(message)
        await client.send_message(message.channel, msg)

    if message.content.startswith('!add'):

        msg = ''
        user = message.content.replace("!add ","")

        #http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=D7C8942FA367B63C7ED53B8D9F287EF0&vanityurl=bobht17


        if 'https://steamcommunity.com/id/' not in user:
            msg = 'That URL is incorrect. Try again you pleb.'
            await client.send_message(message.channel, msg)
        else:
            user = user.replace("https:\/\/steamcommunity.com\/id\/","")
            #Steam_Page = requests.get('http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=D7C8942FA367B63C7ED53B8D9F287EF0&vanityurl=' + user)
           
            data = json.load('http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=D7C8942FA367B63C7ED53B8D9F287EF0&vanityurl=' + user)
            userID = data["response"]["steamid"]
            
            print(userID)

            #if 'no match' in str(steamResult):
           #     msg = 'That user doesn\'t exist.'
           #     await client.send_message(message.channel, msg)
          #  else:
                #{"response":{"steamid":"76561197997859060","success":1}}

            #    userID = str(steamResult).replace("{","").replace("}","").replace("\"","").replace(":","").replace("steamid","").replace("response","")
           #     userID = str(userID).replace("success1","").replace("","")
            
            #    file = open('UserList.txt','a') 
           #    file.write(userID + '\n') 
        
            #    file.close() 
            #    user = ''

             #   msg = 'Thanks, you have been added to the user list.'
            #    await client.send_message(message.channel, msg)             
            
    def formatTimes(inputProduct):
        return str(inputProduct).replace(" ", "").replace("'","").replace("USD","").replace("\\n","").replace(",Add:","")
    
    def ExtractLapTime(UserID, resultElementsTree):
        return resultElementsTree.xpath("//*[contains(text()," + "'" + str(UserID).replace("'","") + "'" + ")]/following::td[2]/span[2]/text()")
        #return resultElementsTree.xpath("//*[@id=" + "'" + str(UserID).replace("'","") + "'" + "]/following::td[2]/span[2]/text()")

    def ExtractTrackName(resultElementsTree):
        return resultElementsTree.xpath("/html/body/div[3]/div[2]/h2/span[2]/span[1]/text()")
    
    if message.content.startswith('!list'):

        msg = '!add [Steam Username] : adds you to the bot to grab times' + '\r\n'
        msg = msg + '!times : List the current standings for the time trial/car combo' + '\r\n'

        await client.send_message(message.channel, '`' + msg + '`')

    if message.content.startswith('!times'):
        try:
            #Track One
            msg = ''

            TT_Page = requests.get('http://cars2-stats-steam.wmdportal.com/index.php/leaderboard?track=1836888499&vehicle=820529698')
            TT_Tree = html.fromstring(TT_Page.content)

            msg = str(ExtractTrackName(TT_Tree)).replace("[","").replace("'","").replace("]","") + ' -- Time Trial Ends April 10th 2018' + '\r\n' 
            msg = msg + 'Vehicle: LMP2 Liegar JS P2 Nissan' + '\r\n \r\n'

            f = open('UserList.txt','r')
            line = f.readline()
            times = {}
            while line != '':
                line = line.strip('\n')
                lapTime = str(ExtractLapTime(line,TT_Tree)).replace("[","").replace("'","").replace("]","")
            
                if len(lapTime) > 1 and len(lapTime) < 10  :
                   times[line] = lapTime

                # use realine() to read next line
                line = f.readline()
            f.close()
        
            test = sorted([(value,key) for (key,value) in times.items()])
            index = 1
            for key, value in test:
                msg = msg + str(index) + ". " + value + ': ' + key + '\r\n'
                index = index + 1

            await client.send_message(message.channel, '`' + msg + '`')

            #End Track One

            msg = ''

            TT_Page2 = requests.get('http://cars2-stats-steam.wmdportal.com/index.php/leaderboard?track=2701023129&vehicle=2459105748')
            TT_Tree2 = html.fromstring(TT_Page2.content)

            msg =  str(ExtractTrackName(TT_Tree2)).replace("[","").replace("'","").replace("]","") + ' -- Time Trial Ends April 20th 2018' + '\r\n' 
            msg = msg + 'Vehicle: Lotus Type 78 Cosworth' + '\r\n \r\n'

            f = open('UserList.txt','r')
            line = f.readline()
            times = {}
            while line != '':
                line = line.strip('\n')
                lapTime = str(ExtractLapTime(line,TT_Tree2)).replace("[","").replace("'","").replace("]","")
            
                if len(lapTime) > 1 and len(lapTime) < 10  :
                   times[line] = lapTime

                # use realine() to read next line
                line = f.readline()
            f.close()

            test = sorted([(value,key) for (key,value) in times.items()])
            index = 1
            for key, value in test:
                msg = msg + str(index) + ". " + value + ': ' + key + '\r\n'
                index = index + 1

            await client.send_message(message.channel, '` `')
            await client.send_message(message.channel, '`' + msg + '`')
        except:
            return True;

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run(TOKEN)
